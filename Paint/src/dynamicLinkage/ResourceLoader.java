package dynamicLinkage;

import java.io.InputStream;

final public class ResourceLoader {

	private static final String packageName = "images";
	public static InputStream load(String path){
	 
	 //works with jar and main
	 InputStream input = ResourceLoader.class.getClassLoader().getResourceAsStream(packageName+"/"+path); 
	 return input;
	}
}
